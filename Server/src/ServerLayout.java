import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Bartek on 2017-05-07.
 */
public class ServerLayout extends Application implements EventHandler<ActionEvent> {


    Stage window;

    Scene scene;

    GridPane grid;

    Button startButton;
    Button stopButton;

    ChatServer cS = new ChatServer();
    public static void main(String[] args)
    {
        launch(args);

    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("MyChat login panel");

        grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);

        startButton = new Button("Start");
        startButton.setOnAction(this);
        GridPane.setConstraints(startButton, 0, 0);
        stopButton=new Button("Stop");
        stopButton.setOnAction(this);
        GridPane.setConstraints(stopButton,1,0);

        grid.getChildren().addAll( startButton,stopButton);
        scene = new Scene(grid, 300, 200);

        window.setScene(scene);
        window.show();
    }

    @Override
    public void handle(ActionEvent event) {
        if(event.getTarget()==startButton)
        {
            try {

              cS.createserver();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(event.getTarget()==stopButton)
        {
            try {
                cS.stopServer();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}


