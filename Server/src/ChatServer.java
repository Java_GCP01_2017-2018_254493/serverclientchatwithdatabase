import DataBase.LoginHistoryMethods;
import DataBase.UsersMethods;

import java.io.*;
import java.util.*;
import java.net.*;
import static java.lang.System.out;

public class ChatServer {

    public static void main(String... args) throws Exception {
       new ChatServer().createserver();
  }
    //private boolean stop = true;
    Vector<String> users = new Vector<String>();
    Vector<Manageuser> clients = new Vector<Manageuser>();
    ServerSocket server;
    public void createserver() throws Exception {
         server = new ServerSocket(80, 10);
        out.println("Now Server Is Running");
        while (true) {
            Socket client = server.accept();
            Manageuser c = new Manageuser(client);
            clients.add(c);
        }
    }

    public void stopServer() throws IOException {
        server.close();
        out.println("Server is closed");
        System.exit(0);
    }

    public void sendtoall(String user, String message) {

        for (Manageuser c : clients) {
            if (!c.getchatusers().equals(user)) {
                c.sendMessage(user, message);
            }
        }
    }

    public void sendtoallWhoIsConnected(String user) {

        for (Manageuser c : clients) {
            if (!c.getchatusers().equals(user)) {
                c.sendWhoIsConectedMessage(user);
            }
        }
    }

    public void sendtoallWhoIsDisConnected(String user) {

        for (Manageuser c : clients) {
            if (!c.getchatusers().equals(user)) {
                c.sendWhoIsDisConectedMessage(user);
            }
        }
    }



    class Manageuser extends Thread {

        String gotuser = "";
        BufferedReader input;
        PrintWriter output;

        public Manageuser(Socket client) throws Exception {

            input = new BufferedReader(new InputStreamReader(client.getInputStream()));
            output = new PrintWriter(client.getOutputStream(), true);

            gotuser = input.readLine();
            System.out.println("Client "+ gotuser + " is connected" );
            LoginHistoryMethods.AddLog("connect", UsersMethods.NumberOfId(gotuser));
            ///////////////Test/////////
            sendtoallWhoIsConnected(gotuser);

            users.add(gotuser); 
            start();
        }

        public void sendMessage(String chatuser, String chatmsg) {
            output.println(chatuser + " Says:" + chatmsg);
        }

        public void sendWhoIsConectedMessage(String chatuser){output.println(chatuser + " is connected");}

        public void sendWhoIsDisConectedMessage(String chatuser){output.println(chatuser + " is disconnected");}

        public String getchatusers() {
            return gotuser;
        }

        @Override
        public void run() {
            String line;
            try {
                while (true) {
                    line = input.readLine();
                    if (line.equals("end")) {
                        System.out.println("Client "+ gotuser + " is disconnect" );
                        LoginHistoryMethods.AddLog("disconnect", UsersMethods.NumberOfId(gotuser));
                        sendtoallWhoIsDisConnected(gotuser);
                        clients.remove(this);
                        users.remove(gotuser);
                        break;
                    }
                    sendtoall(gotuser, line); 
                }
            } 
            catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } 
    } 
} 
