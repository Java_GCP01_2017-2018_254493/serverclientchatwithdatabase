/**
 * Created by Bartek on 2017-05-03.
 */


import DataBase.ClassCreateToPopulateTableView;
import DataBase.UsersMethods;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.web.HTMLEditor;
import javafx.stage.Stage;
import javafx.scene.layout.VBox;
import java.sql.*;
import java.io.IOException;
import java.util.*;


public class LoginLayout   extends Application implements EventHandler<ActionEvent>
{


    TableView<ClassCreateToPopulateTableView> table = new TableView<>();

    LoginProperties lP;

    Stage window;

    Scene scene,mainScene,viewScene,modifyScene;

    GridPane grid;

    GridPane modifyGrid;

    VBox mainLayout;
    VBox  viewGrid;

    Button loginButton;
    Button registerButton;
    Button sendMessage;
    Button showView;
    Button modifyUser;
    Button exit;
    Button modifyButton;
    Button backToChatButton;
    Button backToChatFromModifyWindow;

    Label nameLabel;
    Label passwordLabel;
    Label registerNameLabel;
    Label registerPasswordLabel;
    Label registerRetryPasswordLabel;
    Label modifyNameLabel;
    Label modifyPasswordLabel;

    TextField nameInput;
    TextField passwordInput;
    TextField registerNameInput;
    TextField registerPasswordInput;
    TextField registerRetryPasswordInput;

    TextField modifyNameInput;
    TextField modufyPasswordInput;

    TextField errorsField;
    TextField errors;

//TextField messageInput;
    TextArea messageShow;

    HTMLEditor messageInput;

  public static void main(String[] args)
  {
     launch(args);

  }

    @Override
   public void start(Stage primaryStage) throws Exception {

        System.setProperty("file.encoding", "UTF-8");
        lP=new LoginProperties();
        window = primaryStage;
        window.setTitle("MyChat login panel");

        grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);

        nameLabel = new Label("Username:");
        GridPane.setConstraints(nameLabel, 0, 0);
        passwordLabel = new Label("Password:");
        GridPane.setConstraints(passwordLabel, 0, 1);
        registerNameLabel = new Label("Register username");
        GridPane.setConstraints(registerNameLabel, 3, 0);
        registerPasswordLabel = new Label("Register password");
        GridPane.setConstraints(registerPasswordLabel, 3, 1);
        registerRetryPasswordLabel = new Label("Register retryPassword");
        GridPane.setConstraints(registerRetryPasswordLabel, 3, 2);

        nameInput = new TextField();
        GridPane.setConstraints(nameInput, 1, 0);
        passwordInput = new TextField();
        GridPane.setConstraints(passwordInput, 1, 1);
        registerNameInput =new TextField();
        GridPane.setConstraints(registerNameInput , 4, 0);
        registerPasswordInput=new TextField();
        GridPane.setConstraints(registerPasswordInput , 4, 1);
        registerRetryPasswordInput=new TextField();
        GridPane.setConstraints(registerRetryPasswordInput , 4, 2);
        errorsField=new TextField();
        GridPane.setConstraints(errorsField, 1, 3);

        loginButton = new Button("Log In");
        loginButton.setOnAction(this);
        GridPane.setConstraints(loginButton, 1, 2);
        registerButton=new Button("Register");
        registerButton.setOnAction(this);
        GridPane.setConstraints(registerButton,4,3);

        grid.getChildren().addAll(errorsField,nameLabel, nameInput, passwordInput, passwordLabel, loginButton,registerNameLabel,registerPasswordLabel,registerRetryPasswordLabel,registerNameInput,registerPasswordInput,registerRetryPasswordInput,registerButton);
        scene = new Scene(grid, 600, 200);

/////////////////////MessageSending window////////////////////////////////
        messageInput = new HTMLEditor();
        messageInput.setPrefHeight(200);
        messageShow = new TextArea();
        messageShow.setPrefHeight(550);
        sendMessage = new Button("Send message");
        sendMessage.setOnAction(this);
        sendMessage.setPrefSize(600,20);
        showView = new Button("Show view");
        showView.setOnAction(this);
        showView.setPrefSize(600,20);
        modifyUser = new Button("Modify user");
        modifyUser.setOnAction(this);
        modifyUser.setPrefSize(600,20);
        exit = new Button("LogOut");
        exit.setOnAction(this);
        exit.setPrefSize(600,20);
        mainLayout = new VBox(messageShow, messageInput, sendMessage, exit,showView, modifyUser);
        mainLayout.setPrefSize(600, 600);
        mainScene = new Scene(mainLayout, 600, 600);
        //////////////////////MessageSending window////////////////////////////////



////////////////////////////View Table window////////////////////////////////////////

        TableColumn<ClassCreateToPopulateTableView,String> col1 = new TableColumn<>("ACTIVITY");
        col1.setMinWidth(40);
        col1.setCellValueFactory(new PropertyValueFactory<>("activity"));

        TableColumn<ClassCreateToPopulateTableView, java.sql.Date> col2 = new TableColumn<>("date");
        col2.setMinWidth(40);
        col2.setCellValueFactory(new PropertyValueFactory<>("date"));

        TableColumn<ClassCreateToPopulateTableView,String> col3 = new TableColumn<>("Login");
        col3.setMinWidth(40);
        col3.setCellValueFactory(new PropertyValueFactory<>("login"));

        TableColumn<ClassCreateToPopulateTableView,String>  col4 = new TableColumn<>("Password");
        col4.setMinWidth(40);
        col4.setCellValueFactory(new PropertyValueFactory<>("password"));

        TableColumn<ClassCreateToPopulateTableView,String>  col5 = new TableColumn<>("ROLE");
        col5.setMinWidth(40);
        col5.setCellValueFactory(new PropertyValueFactory<>("role"));

        table.getColumns().addAll(col1, col2, col3, col4, col5);
        backToChatButton=new Button("Back to chat");
        backToChatButton.setOnAction(this);
        backToChatButton.setPrefSize(600,20);
        viewGrid = new VBox(table,backToChatButton);
        viewGrid.setPrefSize(600, 600);
        table.setItems(getView());
        viewScene= new Scene(viewGrid, 600, 600);


////////////////////////////////View Table window /////////////////////////////////


        ////////////////////////////////Modify window /////////////////////////////////
        modifyGrid = new GridPane();
        modifyGrid.setPadding(new Insets(10, 10, 10, 10));
        modifyGrid.setVgap(8);
        modifyGrid.setHgap(10);

        modifyNameLabel= new Label("Changed login");
        GridPane.setConstraints(modifyNameLabel, 0, 0);
        modifyPasswordLabel = new Label("Changed password");
        GridPane.setConstraints(modifyPasswordLabel, 0, 1);
        modifyNameInput = new TextField();
        GridPane.setConstraints(modifyNameInput, 1, 0);
        modufyPasswordInput=new TextField();
        GridPane.setConstraints(modufyPasswordInput, 1, 1);
        modifyButton =  new Button("Modify");
        modifyButton.setOnAction(this);
        GridPane.setConstraints(modifyButton, 1, 2);
        backToChatFromModifyWindow = new Button("Back to chat");
        backToChatFromModifyWindow.setOnAction(this);
        GridPane.setConstraints(backToChatFromModifyWindow, 0, 2);
                errors = new TextField();
        GridPane.setConstraints(errors, 2, 2);

        modifyGrid.getChildren().addAll(errors, modifyNameLabel,modifyPasswordLabel,modifyNameInput,modufyPasswordInput,modifyButton,backToChatFromModifyWindow);
        modifyScene = new Scene(modifyGrid, 600, 200);


        ////////////////////////////////Modify window /////////////////////////////////






        window.setScene(scene);
        window.show();
    }

    /////////////////////Events test//////////////////////////////////
    private java.util.List<AddSendStringListner> addSendStringListnerList= new ArrayList();
    public synchronized  void addToSendStringListnerList(AddSendStringListner listner)
    {
        addSendStringListnerList.add(listner);
    }
    public synchronized  void removeToSendStringListnerList(AddSendStringListner listner )
    {
        addSendStringListnerList.remove(listner);
    }


    /////////////////////End of events test///////////////////////////



    @Override
   public void handle(ActionEvent event) {
       String servername = "localhost";

        if(event.getTarget()==loginButton)
        {
           try {

//               lP.loadProperties();
//               String passwordToCompare = lP.Dencrypt(lP.properties.getProperty(nameInput.getText()));
              if( UsersMethods.CheckIfExist(nameInput.getText(),passwordInput.getText()))
              {
                  new ChatUsers(nameInput.getText(), servername, this);
                  window.setScene(mainScene);
              }
              else
              {
                  System.out.println( "Not working");
              }

           } catch (Exception e) {

               e.printStackTrace();
           }

       }
        if(event.getTarget()==registerButton && registerPasswordInput.getText().equalsIgnoreCase(registerRetryPasswordInput.getText()))
        {

           lP.saveProperties(registerNameInput.getText(),lP.Encrypt(registerPasswordInput.getText()));
            errorsField.setText(UsersMethods.AddUser(registerNameInput.getText(),registerPasswordInput.getText()));
        }
        else
        {
//////////////////////////////Do some kind of warning//////////////////////////////////////////////////////////////
        }
        if(event.getTarget()==sendMessage)
        {
            //////////////////////Test of new messenger button/////////////////////////
            for (AddSendStringListner assl:addSendStringListnerList)
            {
                try {
                    //assl.handle(messageInput.getText());
                    assl.handle(messageInput.getHtmlText());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //messageShow.setHtmlText("Me Says: "+(messageInput.getText()+"\n"));

            messageShow.appendText("Me Says: "+(messageInput.getHtmlText()+"\n"));
            messageInput.setHtmlText("");
            //////////////////////Test of new messenger button/////////////////////////
        }
        if(event.getTarget()==exit)
        {
          //  pw.println("end");
            for (AddSendStringListner assl:addSendStringListnerList)
            {
                try {
                    //assl.handle(messageInput.getText());
                    assl.handle("end");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.exit(0);
        }

        if(event.getTarget()==showView)
        {
            table.setItems(getView());
            window.setScene(viewScene);
        }

        if(event.getTarget()==modifyUser)
        {
            window.setScene(modifyScene);
        }
        if(event.getTarget()==modifyButton)
        {
           if(!modifyNameInput.getText().isEmpty())
           {
              errors.setText(UsersMethods.UpdateUserLogin(UsersMethods.NumberOfId(nameInput.getText()),modifyNameInput.getText()));
           }
            if( !modifyPasswordLabel.getText().isEmpty())
            {
                errors.setText(UsersMethods.UpdateUserPassword(UsersMethods.NumberOfId(nameInput.getText()),modufyPasswordInput.getText()));
            }

        }
        if(event.getTarget()==backToChatButton)
        {
            window.setScene(mainScene);
        }
        if(event.getTarget()==backToChatFromModifyWindow)
        {
            window.setScene(mainScene);
        }


    }
    public ObservableList<ClassCreateToPopulateTableView> getView()
    {
        ObservableList<ClassCreateToPopulateTableView> dataView = FXCollections.observableArrayList();

        try {
            Connection conn = UsersMethods.dbConnector();
            String query = "select * from V1 ";

            PreparedStatement pst = conn.prepareStatement(query);

            ResultSet  rs = pst.executeQuery();

            while (rs.next()) {
                dataView.add(new ClassCreateToPopulateTableView(rs.getString(1),rs.getDate(2),   rs.getString(3),rs.getString(4),rs.getString(5)));

            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return dataView;
    }
}
