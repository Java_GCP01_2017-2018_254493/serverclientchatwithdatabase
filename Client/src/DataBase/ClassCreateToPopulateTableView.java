package DataBase;

import java.util.Date;

/**
 * Created by Bartek on 2017-05-22.
 */
public class ClassCreateToPopulateTableView {


    private String activity;

    private java.sql.Date date;

    private String login;

    private String password;

    private String role;

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(java.sql.Date date) {
        this.date = date;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public ClassCreateToPopulateTableView(String ACTIVITY, java.sql.Date DATE, String Login, String Password, String ROLE)
    {
        activity=ACTIVITY;
date=DATE;
        login=Login;
        password=Password;
        role=ROLE;
    }





}
