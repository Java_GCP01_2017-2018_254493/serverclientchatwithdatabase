package DataBase;


import java.sql.Date;

/**
 * Created by Bartek on 2017-05-22.
 */
public class LoginHistory {

    private int iD;

    private String activity;

    private Date date;

    private int loginHistoryUser;



    public int getiD() {
        return iD;
    }

    public void setiD(int iD) {
        this.iD = iD;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getLoginHistoryUser() {
        return loginHistoryUser;
    }

    public void setLoginHistoryUser(int loginHistoryUser) {
        this.loginHistoryUser = loginHistoryUser;
    }
}
