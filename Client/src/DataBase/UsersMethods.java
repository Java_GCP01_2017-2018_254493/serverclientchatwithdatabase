package DataBase; /**
 * Created by Bartek on 2017-05-16.
 */



import java.sql.*;


public class UsersMethods {


    public static Connection dbConnector() {
        try {
            Class.forName("org.sqlite.JDBC");
            Connection conn=DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Bartek\\Documents\\LoginDB.sqlite");
            System.out.println("Connection is sucesfull");
            return conn;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static String AddUser(String login,String password) {

        String resoult;
        if(ValidationClass.LoginValidation(login)==null && ValidationClass.PasswordValidation(password)==null ) {
            try {
                Connection conn = UsersMethods.dbConnector();
                PreparedStatement statement = conn.prepareStatement(
                        "INSERT INTO USERS (Login,Password) VALUES (?,?)");
                statement.setString(1, login);
                statement.setString(2, password);
                statement.executeUpdate();


                conn.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            resoult="You are registerd";
            return resoult;
        }
        else
        return ValidationClass.LoginValidation(login) + " "+ ValidationClass.PasswordValidation(password) ;
    }

    public static void DeleteUser(int ID) {
        try
        {
            Connection conn = UsersMethods.dbConnector();
            PreparedStatement statement = conn.prepareStatement(
                    "DELETE  FROM Roles WHERE RolesUser = ?");
            statement.setInt(1,ID);
            statement.executeUpdate();

            PreparedStatement statement1 = conn.prepareStatement(
                    "DELETE  FROM LoginHistory WHERE LoginHistoryUser = ?");
            statement1.setInt(1,ID);
            statement1.executeUpdate();

            PreparedStatement statement2 = conn.prepareStatement(
                    "DELETE FROM USERS WHERE ID = ?");
            statement2.setInt(1,ID);
            statement2.executeUpdate();

            conn.close();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }


    }

    public static String UpdateUserLogin(int ID,String ChangedLogin) {

        if(ValidationClass.LoginValidation(ChangedLogin)==null ) {
            try {
                Connection conn = UsersMethods.dbConnector();
                PreparedStatement statement = conn.prepareStatement(
                        "UPDATE USERS SET Login =? WHERE ID = ?");
                statement.setString(1, ChangedLogin);
                statement.setInt(2, ID);
                statement.executeUpdate();

                conn.close();

            } catch (Exception ex) {
                ex.printStackTrace();
                return "You modify Login";
            }
        }
        return ValidationClass.LoginValidation(ChangedLogin);

    }

    public static String UpdateUserPassword(int ID,String ChangedPassword) {

        if(ValidationClass.PasswordValidation(ChangedPassword)==null ) {
            try {
                Connection conn = UsersMethods.dbConnector();
                PreparedStatement statement = conn.prepareStatement(
                        "UPDATE USERS SET Password =? WHERE ID = ?");
                statement.setString(1, ChangedPassword);
                statement.setInt(2, ID);
                statement.executeUpdate();

                conn.close();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return "You modify password";
        }
        else
        return ValidationClass.PasswordValidation(ChangedPassword);
    }

    public static boolean CheckIfExist(String Login,String Password)
    {
        ResultSet rs;
        boolean b =true;
        try
        {
            Connection conn = UsersMethods.dbConnector();
           // String query = "select DISTINCT Login, Password from V1 where Login=? and Password=? ";
           String query = "select DISTINCT Login, Password from USERS where Login=? and Password=? ";
            PreparedStatement pst = conn.prepareStatement(query);

            pst.setString(1, Login);
            pst.setString(2, Password);
            rs = pst.executeQuery();
            int count = 0;
            while (rs.next()) {
                count++;
                //System.out.println(rs.getString("Login"));
            }
            if (count == 1) {
                b = true;
                rs.close();
                pst.close();
                return b;
            } else
                {
                b = false;
                    rs.close();
                    pst.close();
                return b;
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return b;
    }

    public static int NumberOfId(String Login)
    {
        ResultSet rs=null;
        int resoult=0;
        try {
            Connection conn = UsersMethods.dbConnector();
            String query = "select ID  from USERS where Login=?";

            PreparedStatement pst = conn.prepareStatement(query);

            pst.setString(1, Login);
            rs = pst.executeQuery();

           resoult= rs.getInt(1);
            rs.close();
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

       return resoult;
    }


}
