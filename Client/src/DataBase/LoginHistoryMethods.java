package DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * Created by Bartek on 2017-05-22.
 */
public class LoginHistoryMethods {


    public static Connection dbConnector() {
        try {
            Class.forName("org.sqlite.JDBC");
            Connection conn= DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Bartek\\Documents\\LoginDB.sqlite");
            System.out.println("Connection is sucesfull");
            return conn;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void AddLog(String Activity,int LoginHistoryUser) {
        try
        {
            Connection conn = UsersMethods.dbConnector();
            PreparedStatement statement = conn.prepareStatement(
                    "INSERT INTO LoginHistory (ACTIVITY,LoginHistoryUser) VALUES (?,?)");
            statement.setString(1,Activity);
            statement.setInt(2,LoginHistoryUser);
            statement.executeUpdate();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static void DeleteLog(int ID) {
        try
        {
            Connection conn = UsersMethods.dbConnector();


            PreparedStatement statement = conn.prepareStatement(
                    "DELETE  FROM LoginHistory WHERE ID = ?");
            statement.setInt(1,ID);
            statement.executeUpdate();

            conn.close();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }



}
