package DataBase;

/**
 * Created by Bartek on 2017-05-22.
 */
public class Roles {

    private int iD;

    private String role;

    private String rolesUser;


    public int getiD() {
        return iD;
    }

    public void setiD(int iD) {
        this.iD = iD;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRolesUser() {
        return rolesUser;
    }

    public void setRolesUser(String rolesUser) {
        this.rolesUser = rolesUser;
    }
}
