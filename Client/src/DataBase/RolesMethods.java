package DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * Created by Bartek on 2017-05-22.
 */
public class RolesMethods {

    public static Connection dbConnector() {
        try {
            Class.forName("org.sqlite.JDBC");
            Connection conn= DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Bartek\\Documents\\LoginDB.sqlite");
            System.out.println("Connection is sucesfull");
            return conn;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void AddRole (String Role,int RoleUser) {
        try
        {
            Connection conn = UsersMethods.dbConnector();
            PreparedStatement statement = conn.prepareStatement(
                    "INSERT INTO Roles (ROLE,RolesUser) VALUES (?,?)");
            statement.setString(1,Role);
            statement.setInt(2,RoleUser);
            statement.executeUpdate();
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }


    public static void DeleteRole(int ID) {
        try
        {
            Connection conn = UsersMethods.dbConnector();


            PreparedStatement statement = conn.prepareStatement(
                    "DELETE  FROM Roles WHERE ID = ?");
            statement.setInt(1,ID);
            statement.executeUpdate();

            conn.close();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }


    public static void UpdateRole(int ID,String ChangedRole) {
        try
        {
            Connection conn = UsersMethods.dbConnector();
            PreparedStatement statement = conn.prepareStatement(
                    "UPDATE Roles SET ROLE =? WHERE ID = ?");
            statement.setString(1,ChangedRole);
            statement.setInt(2,ID);
            statement.executeUpdate();

            conn.close();

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
