package DataBase;

/**
 * Created by Bartek on 2017-05-22.
 */
public class User {

    private int iD;

    private String login;

    private String password;

    public User(String Login,String Password)
    {
        login=Login;
        password=Password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
