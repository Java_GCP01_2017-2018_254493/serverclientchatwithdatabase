import java.io.IOException;

/**
 * Created by Bartek on 2017-05-03.
 */
public interface AddSendStringListner {
    void handle(String var1) throws IOException;
}
